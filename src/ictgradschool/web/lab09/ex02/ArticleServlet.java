package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ArticleServlet")
public class ArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String articleTitle = request.getParameter("articleTitle");
        String articleAuthor = request.getParameter("articleAuthor");
        String content = request.getParameter("content");
        String option = request.getParameter("option");
        String[] array = new String[]{"Four", "Three", "Two", "One"};

        PrintWriter out = response.getWriter();
        // printWriter is used to write to the HTML document
        out.println("<!doctype html>");
        out.println("<br>");
        out.println("<h1>" + articleTitle + "</h1>");
        out.println("<br>");
        out.println("<br>");
        out.println("by: " + articleAuthor);
        out.println("<br>");
        out.println("Genre: " + option);
        out.println("<br>");
        out.println("<br>");
        out.println(content);


        out.println("<h2>This is a string array from Java (via Post):</h2>");
//        out.println("<br>");

          out.println("<ul>");

        for (int i = 0; i < array.length; i++) {
            out.println("<li>" + array[i] + "</li>");
        }

        out.println("</ul>");
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

}
